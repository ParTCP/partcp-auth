<?php

$Conf['base_url'] = 'https://my-domain.com/oidc-connector';
$Conf['path_to_keys'] = __DIR__ . '/.keys';

$Conf['oidc_client_id'] = 'my_oidc_client';
$Conf['oidc_client_secret'] = 'abcd-1234';
$Conf['oidc_base_url'] = 'https://login.my-domain.com/auth/realms/diebasis';

$Conf['partcp_server'] = 'partcp.my-domain.com';
$Conf['partcp_id'] = 'oidc-connector@partcp.my-domain.com';

$Conf['smtp_host'] = '';
$Conf['smtp_port'] = 587;
$Conf['smtp_username'] = '';
$Conf['smtp_password'] = '';
$Conf['smtp_security'] = 'tls';

$Conf['callback_profile2id'] = function( $profile ){
	return 'p' . $profile->id;
};

$Conf['html_finish_login'] = <<<EOT
<title>Successful Login</title>
<h1>Successful Login</h1>
<p><button onclick='self.close()'>Proceed</button></p>
EOT;

$Conf['html_finish_logout'] = <<<EOT
<title>Successful Logout</title>
<h1>Successful Logout</h1>
<p><button onclick='self.close()'>Proceed</button></p>
EOT;

$Conf['html_finish_consent'] = <<<EOT
<title>Successful Consent</title>
<h1>Successful Consent</h1>
<p><button onclick='self.close()'>Proceed</button></p>
EOT;

$Conf['privacy_title'] = 'Einwilligung in die Datenspeicherung und -verarbeitung';
$Conf['privacy_button'] = 'Weiter';

$Conf['callback_profile2email'] = function( $profile ){
	$profile = (object) $profile;
	return $profile->email ?? NULL;
};

$Conf['email_notification_sender'] = '';
$Conf['email_notification_subject'] = 'Neuer Berechtigungscode ausgestellt';
$Conf['email_notification_body'] = <<<EOT
Für die Teilnehmerkennung %id% wurde ein neuer Berechtigungscode für die Hinterlegung
eines Schlüssels ausgestellt. Falls Du dies nicht veranlasst hast, setz Dich bitte
umgehend mit einem Administrator in Verbindung.
EOT;
// end of file config.php

