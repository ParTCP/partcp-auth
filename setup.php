<?php

/*
	OIDC Client for ParTCP Key Management
	Copyright (C) 2022-2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require_once 'config.php';

if ( empty( $Conf['path_to_keys'] ) ){
	die( "Configuration parameter path_to_keys not speficied\n" );
}

if ( empty( $Conf['partcp_id'] ) ){
	die( "Configuration parameter partcp_id not speficied\n" );
}

if ( empty( $Conf['partcp_credential'] ) ){
	die( "Configuration parameter partcp_credential not speficied\n" );
}

require_once 'lib/partcp-php/partcp.class.php';
$ParTCP = new ParTCP( $Conf['path_to_keys'] );

if ( ! $ParTCP->set_local_id( $Conf['partcp_id'], TRUE, TRUE ){
	die( "ERROR: {$ParTCP->lastError}\n" );
}

$response = yaml_parse( $result['body'] );

echo "Setup finished successfully\n";

