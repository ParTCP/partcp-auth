<!doctype html>
<html lang="en">
<head>
	<!--
		OIDC Client for ParTCP Key Management
		Copyright (C) 2022-2024 Martin Wandelt

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU Affero General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU Affero General Public License for more details.

		You should have received a copy of the GNU Affero General Public License
		along with this program.  If not, see <https://www.gnu.org/licenses/>
	-->
	<meta charset="utf-8">
	<title>Schwarmportal-Schlüsselmanager</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
	<div class="container">
		<h1 class="mt-5 mb-5">Schwarmportal-Schlüsselmanager</h1>
		<?php

		require_once 'config.php';
		require_once 'lib/partcp-php/partcp.class.php';
		$ParTCP = new ParTCP( $Conf['path_to_keys'] );
		$ParTCP->set_local_id( $Conf['partcp_id'] );
		$ParTCP->set_remote_id( $Conf['partcp_server'] );
		$idField = $Conf['oidc_id_field'];
		session_start();

		if ( isset( $_GET['logout'] ) ){
			unset( $_SESSION['username'] );
			unset( $_SESSION['sub'] );
			unset( $_SESSION['profile'] );
			$url = urlencode( "{$Conf['base_url']}/index.php" );
			header( "Location: {$Conf['oidc_base_url']}/protocol/openid-connect/logout?redirect_uri={$url}" );
			die();
		}

		if ( isset( $_GET['register'] ) ){
			if ( empty( $_SESSION['profile']->$idField ) ){
				echo "<p style='color:red'>Keine Mitgliedsnummer hinterlegt!</p>\n";
				die();
			}
			$id = $Conf['callback_profile2id']( $_SESSION['profile'] );
			$options = [];
			if ( ! empty( $_SESSION['consent_statement'] ) && ! empty( $_SESSION['consented'] ) ){
				$options['consent_statement'] = $_SESSION['consent_statement'];
			}
			$credential = $ParTCP->register_participant( $id, $options );
			echo "<div class='container'>\n";
			echo "<div class='row'>\n";
			echo "<div class='col-sm-6 ml-auto mr-auto'>\n";
			if ( $credential ){
				if ( is_callable( $Conf['callback_profile2email'] ) ){
					$email = $Conf['callback_profile2email']( $_SESSION['profile'] );
					if ( $email && ! empty( $Conf['smtp_host'] )
						&& ! empty( $Conf['email_notification_subject'] )
					){
						require_once __DIR__ . '/lib/smtp_mailer.class.php';
						$smtp = new SMTP_Mailer( $Conf['smtp_host'], $Conf['smtp_port'],
							$Conf['smtp_username'], $Conf['smtp_password'], $Conf['smtp_security'] );
						$body = str_replace( '%id%', $id, $Conf['email_notification_body'] );
						$smtp->mail( $email, $Conf['email_notification_subject'], $body,
							$Conf['email_notification_sender'] );
					}
				}
				echo "<div class='alert alert-success mt-5 text-center' role='alert'>\n";
				echo "<p>Teilnehmer {$id} wurde erfolgreich registriert. Der Aktivierungscode lautet:</p>\n";
				echo "<h4>{$credential}</h4>\n";
				echo "<p>Bitte notiere Dir den Aktivierungscode. Du brauchst ihn, um die Schwarmportal-App einzurichten.</p>\n";
				echo "<p><a class='btn btn-success' href='{$Conf['base_url']}'>Fertig</a></p>\n";
				echo "</div>\n";
			}
			else {
				echo "<div class='alert alert-danger mt-5 text-center' role='alert'>\n";
				echo "<p><b>Registrierung fehlgeschlagen</b></p>\n";
				echo "<p>{$ParTCP->lastError}</p>\n";
				echo "<p><a class='btn btn-danger' href='{$Conf['base_url']}'>zurück</a></p>\n";
				echo "</div>\n";
				echo "<pre class='mt-5'><code class='text-muted'>{$ParTCP->lastRequest}</code></pre>\n";
			}
			echo "</div>\n";
			echo "</div>\n";
			echo "</div>\n";
			die();
		}

		if ( isset( $_GET['renewal'] ) ){
			if ( empty( $_SESSION['profile']->$idField ) ){
				echo "<p style='color:red'>Keine Mitgliedsnummer hinterlegt!</p>\n";
				die();
			}
			$id = $Conf['callback_profile2id']( $_SESSION['profile'] );
			$credential = $ParTCP->permit_key_renewal( $id );
			echo "<div class='container'>\n";
			echo "<div class='row'>\n";
			echo "<div class='col-sm-6 ml-auto mr-auto'>\n";
			if ( $credential ){
				if ( is_callable( $Conf['callback_profile2email'] ) ){
					$email = $Conf['callback_profile2email']( $_SESSION['profile'] );
					if ( $email && ! empty( $Conf['smtp_host'] )
						&& ! empty( $Conf['email_notification_subject'] )
					){
						require_once __DIR__ . '/lib/smtp_mailer.class.php';
						$smtp = new SMTP_Mailer( $Conf['smtp_host'], $Conf['smtp_port'],
							$Conf['smtp_username'], $Conf['smtp_password'], $Conf['smtp_security'] );
						$body = str_replace( '%id%', $id, $Conf['email_notification_body'] );
						$smtp->mail( $email, $Conf['email_notification_subject'], $body,
							$Conf['email_notification_sender'] );
					}
				}
				echo "<div class='alert alert-success mt-5 text-center' role='alert'>\n";
				echo "<p>Der neue Aktivierungscode für Teilnehmer {$id} wurde erfolgreich erstellt.</p>\n";
				echo "<h4>{$credential}</h4>\n";
				echo "<p>Bitte notiere Dir den Aktivierungscode. Du brauchst ihn, um die Schwarmportal-App einzurichten.</p>\n";
				echo "<p><a class='btn btn-success' href='{$Conf['base_url']}'>Fertig</a></p>\n";
				echo "</div>\n";
			}
			else {
				echo "<div class='alert alert-danger mt-5 text-center' role='alert'>\n";
				echo "<p><b>Beim Erstellen des Aktivierungscodes ist ein Fehler aufgetreten.</b></p>\n";
				echo "<p>{$ParTCP->lastError}</p>\n";
				echo "<p><a class='btn btn-danger' href='{$Conf['base_url']}'>zurück</a></p>\n";
				echo "</div>\n";
				echo "<pre class='mt-5'><code class='text-muted'>{$ParTCP->lastRequest}</code></pre>\n";
			}
			echo "</div>\n";
			echo "</div>\n";
			echo "</div>\n";
			die();
		}

		if ( isset( $_SESSION['sub'] ) ){
			echo "<div class='card'>\n";
			echo "<div class='card-header'>\n";
			echo "<a class='btn btn-sm btn-secondary float-right' href='{$Conf['base_url']}/index.php?logout' role='button'>Ausloggen</a>\n";
			echo "Eingeloggt als <b>{$_SESSION['username']}</b>";
			echo "</div>\n";
			echo "<div class='card-body'>\n";
			echo "<h4>Mitgliedsdaten</h4>\n";
			echo "<p>Mitgl.-Nr.: {$_SESSION['profile']->$idField}</p>\n";
			echo "<p>Name: {$_SESSION['profile']->name}</p>\n";
			echo "<p>E-Mail: {$_SESSION['profile']->email}</p>\n";
			echo "</div>\n";
			echo "</div>\n";
			$ptcpId = $Conf['callback_profile2id']( $_SESSION['profile'] );
			$participant = $ParTCP->get_participant_details( $ptcpId );
			echo "<div class='card mt-5'>\n";
			echo "<div class='card-header'>\n";
			echo "Schwarmportal-Status";
			echo "</div>\n";
			echo "<div class='card-body'>\n";
			if ( ! $participant ){
				if ( strpos( $ParTCP->lastError, '42' ) ){
					echo "<p>Teilnehmer <b>{$ptcpId}</b> ist noch nicht registriert.</p>\n";
					if ( ! isset( $_SESSION['consent_statement'] ) ){
						$server = $ParTCP->get_server_details();
						$_SESSION['consent_statement'] = $server['consent_statement'] ?? '';
						$_SESSION['privacy_notice'] = $server['privacy_notice'] ?? '';
					}
					if ( ! empty( $_SESSION['consent_statement'] ) && empty( $_SESSION['consented'] ) ){
						$url = "{$Conf['base_url']}/privacy.php";
					}
					else {
						$url = "{$Conf['base_url']}/index.php?register";
					}
					echo "<p><a class='btn btn-primary' href='{$url}'>Jetzt registrieren</a></p>\n";
				}
				else {
					echo "<p style='color:red'>Statusabfrage fehlgeschlagen</p>\n";
					echo "<p style='color:red'>{$ParTCP->lastError}</p>\n";
				}
			}
			else {
				if ( empty( $participant['public_key'] ) ){
					echo "<p>Teilnehmer <b>{$ptcpId}</b> ist registriert, hat aber noch keinen öffentlichen Schlüssel hinterlegt.</p>\n";
					echo "<p>Bitte verwende die Schwarmportal-App, um die Registrierung abzuschließen.</p>\n";
					echo "<p>Falls Du den Aktivierungscode verloren hast, kannst Du hier einen neuen anfordern: ";
					echo "<p><a class='btn btn-primary' href='{$Conf['base_url']}/index.php?renewal'>Aktivierungscode anfordern</a></p>\n";
				}
				else {
					echo "<p>Teilnehmer <b>{$ptcpId}</b> ist registriert und hat folgenden öffentlichen Schlüssel hinterlegt:</p>\n";
					echo "<p><code>{$participant['public_key']}</code></p>\n";
					echo "<p>Du hast die Möglichkeit, einen neuen Schlüssel zu hinterlegen.";
					echo "<p><a class='btn btn-primary' href='{$Conf['base_url']}/index.php?renewal'>Neuen Schlüssel hinterlegen</a></p>\n";
				}
			}
			echo "</div>\n";
			echo "</div>\n";
			die();
		}

		$metadata = http_request( "{$Conf['oidc_base_url']}/.well-known/openid-configuration" );

		if ( ! isset( $_GET['code'] ) ){

			$_SESSION['state'] = bin2hex( random_bytes(5) );
			$_SESSION['code_verifier'] = bin2hex( random_bytes(50) );
			$codeChallenge = base64_urlencode( hash( 'sha256', $_SESSION['code_verifier'], TRUE ) );
			$authorizeUrl = $metadata->authorization_endpoint . '?' . http_build_query([
				'response_type' => 'code',
				'client_id' => $Conf['oidc_client_id'],
				'redirect_uri' => $Conf['base_url'],
				'state' => $_SESSION['state'],
				'scope' => 'openid profile',
				'code_challenge' => $codeChallenge,
				'code_challenge_method' => 'S256',
			]);

			echo "<div class='container'>\n";
			echo "<div class='row'>\n";
			echo "<div class='col-sm-6 ml-auto mr-auto'>\n";
			echo "<div class='alert alert-warning mt-5 text-center' role='alert'>\n";
			echo "<p>Das Gerät, das Du gerade benutzt, ist mit keinem Benutzerkonto verknüpft.\n";
			echo "Bitte logge Dich ein, damit Deine Mitgliedsdaten abgerufen werden können.</p>\n";
			echo "<p><a class='btn btn-primary' href='{$authorizeUrl}'>Einloggen</a></p>\n";
			echo "</div>\n";
			echo "</div>\n";
			echo "</div>\n";
			echo "</div>\n";

		}
		else {

			if ( empty( $_SESSION['state'] ) || $_SESSION['state'] != $_GET['state'] ){
				die('Authorization server returned an invalid state parameter');
			}

			if ( isset( $_GET['error'] ) ){
				die( 'Authorization server returned an error: ' . htmlspecialchars( $_GET['error'] ) );
			}

			$response = http_request( $metadata->token_endpoint, [
				'grant_type' => 'authorization_code',
				'code' => $_GET['code'],
				'redirect_uri' => $Conf['base_url'],
				'client_id' => $Conf['oidc_client_id'],
				'client_secret' => $Conf['oidc_client_secret'],
				'code_verifier' => $_SESSION['code_verifier'],
			]);

			if ( ! isset( $response->access_token ) ){
				die('Error fetching access token');
			}

			$userinfo = http_request( $metadata->userinfo_endpoint, [
				'access_token' => $response->access_token,
			]);

			if ( $userinfo->sub ){
				$_SESSION['sub'] = $userinfo->sub;
				$_SESSION['username'] = $userinfo->preferred_username;
				$_SESSION['profile'] = $userinfo;
				header("Location: {$Conf['base_url']}");
				die();
			}

		}



		// Base64-urlencoding is a simple variation on base64-encoding
		// Instead of +/ we use -_, and the trailing = are removed.
		function base64_urlencode( $string ){
			return rtrim( strtr( base64_encode( $string ), '+/', '-_' ), '=' );
		}


		function generate_code( $args = [] ){
			$charList = (string) ( $args['char_list'] ?? 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789' );
			$finalLength = (int) ( $args['final_length'] ?? 16 );
			$groupLength = $args['group_length'] ?? 4;
			$groupSeparator = $args['group_separator'] ?? '-'; 
			$crcLength = $args['crc_length'] ?? 0;
			$netLength = $finalLength - $crcLength;
			$charListLength = strlen( $charList ) - 1;
			$code = '';
			for ( $x = 1; $x <= $netLength; $x++ ){
				$code .= $charList[ rand( 0, $charListLength ) ];
			}
			if ( $groupLength ){
				$code .= str_repeat( ' ', $crcLength );
				$code = substr( chunk_split( $code, $groupLength, $groupSeparator ), 0, -1 );
				$code = rtrim( $code, ' ' );
			}
			if ( $crcLength ){
				$code .= substr( crc32( $code ), - $crcLength );
			}
			elseif ( $groupLength ){
				$code = rtrim( $code, $groupSeparator );
			}
			return $code;
		}


		function http_request( $url, $params = FALSE ){
			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
			if ( $params ){
				curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $params ) );
			}
			return json_decode( curl_exec( $ch ) );
		}


		?>
	</div>
</body>
</html>
