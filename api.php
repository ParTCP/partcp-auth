<?php

/*
	OIDC Client for ParTCP Key Management
	Copyright (C) 2022-2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

function base64_urlencode( $string ){
	return rtrim( strtr( base64_encode( $string ), '+/', '-_' ), '=' );
}


function generate_code( $args = [] ){
	$charList = (string) ( $args['char_list'] ?? 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789' );
	$finalLength = (int) ( $args['final_length'] ?? 16 );
	$groupLength = $args['group_length'] ?? 4;
	$groupSeparator = $args['group_separator'] ?? '-';
	$crcLength = $args['crc_length'] ?? 0;
	$netLength = $finalLength - $crcLength;
	$charListLength = strlen( $charList ) - 1;
	$code = '';
	for ( $x = 1; $x <= $netLength; $x++ ){
		$code .= $charList[ rand( 0, $charListLength ) ];
	}
	if ( $groupLength ){
		$code .= str_repeat( ' ', $crcLength );
		$code = substr( chunk_split( $code, $groupLength, $groupSeparator ), 0, -1 );
		$code = rtrim( $code, ' ' );
	}
	if ( $crcLength ){
		$code .= substr( crc32( $code ), - $crcLength );
	}
	elseif ( $groupLength ){
		$code = rtrim( $code, $groupSeparator );
	}
	return $code;
}


function http_request( $url, $params = FALSE ){
	$ch = curl_init( $url );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
	if ( $params ){
		curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $params ) );
	}
	return json_decode( curl_exec( $ch ) );
}


function debug_output ( $output ){
	global $Conf;
	if ( ! empty( $Conf['debugging'] ) ){
		file_put_contents( __DIR__ . '/debug.log', "{$output}\n", FILE_APPEND );
	}
}


### MAIN SCRIPT ###

if ( $_SERVER['REQUEST_METHOD'] == 'OPTIONS' ){
	die('ok');
}

require_once 'config.php';
require_once 'lib/partcp-php/partcp.class.php';
debug_output( "---\n" . date( 'Y-m-d H:i:s' ) . ' - ' . $_SERVER['REMOTE_ADDR'] );
debug_output( var_export( $_REQUEST, TRUE ) );
$ParTCP = new ParTCP( $Conf['path_to_keys'] );
$ParTCP->set_local_id( $Conf['partcp_id'] );
$ParTCP->set_remote_id( $Conf['partcp_server'] );
$idField = $Conf['oidc_id_field'];
$BaseUrl = $Conf['base_url'] . '/api.php';
header('Content-Type: application/json');
ini_set( 'session.use_cookies', 0 );
if ( ! empty( $_GET['sid'] ) ){
	session_id( $_GET['sid'] );
}
session_start();
$SessionId = session_id();
debug_output( "Session {$SessionId} - " . var_export( $_SESSION, TRUE ) );


### Handle logout request ###

if ( isset( $_GET['logout'] ) ){
	unset( $_SESSION['username'] );
	unset( $_SESSION['sub'] );
	unset( $_SESSION['profile'] );
	$uri = "{$Conf['oidc_base_url']}/protocol/openid-connect/logout";
	if ( isset( $Conf['html_finish_logout'] ) ){
		$uri .= '?redirect_uri=' . urlencode( "{$BaseUrl}?finish_logout&sid={$SessionId}" );
	}
	die( json_encode( [ 'redirect' => $uri	] ) );
}


### Handle finish_logout request ###

if ( isset( $_GET['finish_logout'] ) ){
		header('Content-Type: text/html');
			die( $Conf['html_finish_logout'] ?? '' );
}


### Handle register and renewal request ###

if ( isset( $_GET['register'] ) || isset( $_GET['renewal'] ) ){
	if ( empty( $_SESSION['profile']->$idField ) ){
		debug_output('ERROR: Missing ID property in profile');
		die( json_encode( [ 'error' => 'Missing ID property in profile' ] ) );
	}
	if ( isset( $_GET['register'] ) ){
		if ( ! isset( $_SESSION['consent_statement'] ) ){
			$response = $ParTCP->get_server_details();
			$_SESSION['consent_statement'] = $response['server']['consent_statement'] ?? '';
			$_SESSION['privacy_notice'] = $response['server']['privacy_notice'] ?? '';
		}
		if ( ! empty( $_SESSION['consent_statement'] ) && empty( $_SESSION['consented'] ) ){
			debug_output('REDIRECT TO PRIVACY PAGE');
			die( json_encode( [
				'redirect' => "{$Conf['base_url']}/privacy.php?sid={$SessionId}",
				'url_register' => "{$BaseUrl}?register&sid={$SessionId}",
			]));
		}
	}
	$id = $Conf['callback_profile2id']( $_SESSION['profile']->$idField );
	if ( isset( $_GET['register'] ) ){
		$options = [];
		if ( isset( $_GET['register'] ) && ! empty( $_SESSION['consent_statement'] ) ){
			$options['consent_statement'] = $_SESSION['consent_statement'];
		}
		$credential = $ParTCP->register_participant( $id, $options );
	}
	else {
		$credential = $ParTCP->permit_key_renewal( $id );
	}
	if ( ! $credential ){
		debug_output( $ParTCP->lastError );
		debug_output( $ParTCP->lastRequest );
		die( json_encode( [
			'error' => $ParTCP->lastError,
			'debug_info' => $ParTCP->lastRequest
		]));
	}
	if ( is_callable( $Conf['callback_profile2email'] ) ){
		$email = $Conf['callback_profile2email']( $_SESSION['profile'] );
		if ( $email && ! empty( $Conf['smtp_host'] )
			&& ! empty( $Conf['email_notification_subject'] )
		){
			require_once __DIR__ . '/lib/smtp_mailer.class.php';
			$smtp = new SMTP_Mailer( $Conf['smtp_host'], $Conf['smtp_port'],
				$Conf['smtp_username'], $Conf['smtp_password'], $Conf['smtp_security'] );
				$body = str_replace( '%id%', $id, $Conf['email_notification_body'] );
				$smtp->mail( $email, $Conf['email_notification_subject'], $body,
					$Conf['email_notification_sender'] );
		}
	}
	debug_output( "RETURN participant_id: {$id}, credential: {$credential}" );
	die( json_encode( [
		'participant_id' => $id,
		'credential' => $credential
	]));
}


### There's an active session - deliver status information ###

if ( isset( $_SESSION['sub'] ) ){
	$data = [
		'username' => $_SESSION['username'],
		'profile' => $_SESSION['profile'],
		'participant_id' => $Conf['callback_profile2id']( $_SESSION['profile'] ),
		'url_status' => "{$BaseUrl}?sid={$SessionId}",
		'url_logout' => "{$BaseUrl}?logout&sid={$SessionId}",
		'url_register' => "{$BaseUrl}?register&sid={$SessionId}",
		'url_renewal' => "{$BaseUrl}?renewal&sid={$SessionId}",
	];
	$participant = $ParTCP->get_participant_details( $data['participant_id'] );
	if ( ! $participant ){
		if ( strpos( $ParTCP->lastError, '42' ) ){
			$data['participant_status'] = 'unregistered';
		}
		else {
			$data['participant_status'] = 'unknown';
			$data['error'] = $ParTCP->lastError;
			$data['debug_info'] = $ParTCP->lastRequest;
		}
	}
	else {
		if ( empty( $participant['public_key'] ) ){
			$data['participant_status'] = 'incomplete';
		}
		else {
			$data['participant_status'] = 'complete';
			$data['public_key'] = $participant['public_key'];
		}
	}
	die( json_encode( $data ) );
}


### Finish authorization process ###

if ( isset( $_GET['code'] ) ){
	if ( empty( $_SESSION['state'] ) ){
		die( json_encode( [
			'error' => 'Invalid session'
		]));
	}
	if ( $_SESSION['state'] != $_GET['state'] ){
		die( json_encode( [
			'error' => 'Authorization server returned an invalid state parameter'
		]));
	}
	if ( isset( $_GET['error'] ) ){
		die( json_encode( [
			'error' => "Authorization server returned an error: {$_GET['error']}"
		]));
	}
	$metadata = http_request( "{$Conf['oidc_base_url']}/.well-known/openid-configuration" );
	$response = http_request( $metadata->token_endpoint, [
		'grant_type' => 'authorization_code',
		'code' => $_GET['code'],
		'redirect_uri' => "{$BaseUrl}?sid={$SessionId}",
		'client_id' => $Conf['oidc_client_id'],
		'client_secret' => $Conf['oidc_client_secret'],
		'code_verifier' => $_SESSION['code_verifier'],
	]);
	if ( ! isset( $response->access_token ) ){
		die( json_encode( [
			'error' => 'Error fetching access token'
		]));
	}
	$userinfo = http_request( $metadata->userinfo_endpoint, [
		'access_token' => $response->access_token,
	]);
	if ( $userinfo->sub ){
		$_SESSION['sub'] = $userinfo->sub;
		$_SESSION['username'] = $userinfo->preferred_username;
		$_SESSION['profile'] = $userinfo;
		header('Content-Type: text/html');
		die( $Conf['html_finish_login'] ?? '' );
	}
}

### Start authorization process (if not started before) ###

if ( empty( $_SESSION['state'] ) ){
	$_SESSION['state'] = bin2hex( random_bytes(5) );
	$_SESSION['code_verifier'] = bin2hex( random_bytes(50) );
	$codeChallenge
		= base64_urlencode( hash( 'sha256', $_SESSION['code_verifier'], TRUE ) );
	$metadata = http_request( "{$Conf['oidc_base_url']}/.well-known/openid-configuration" );
	$authorizeUrl = $metadata->authorization_endpoint . '?' . http_build_query([
		'response_type' => 'code',
		'client_id' => $Conf['oidc_client_id'],
		'redirect_uri' => "{$BaseUrl}?sid={$SessionId}",
		'state' => $_SESSION['state'],
		'scope' => 'openid profile',
		'code_challenge' => $codeChallenge,
		'code_challenge_method' => 'S256',
	]);
	die( json_encode( [
		'redirect' => $authorizeUrl,
		'url_status' => "{$BaseUrl}?sid={$SessionId}",
	]));
}


### Nothing to do - return URL for requesting status

die( json_encode( [
	'url_status' => "{$BaseUrl}?sid={$SessionId}",
]));

// end of file api.php

