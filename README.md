# OIDC Client for ParTCP Key Management

This is both a web frontend and a JSON API for managing participant's public
keys on a ParTCP server. A user authenticates via OIDC and can then see his
registration status on the ParTCP server. If not registered yet, the client
will initiate the registration process. For registered users the client offers
the possibility to submit a new public key.


## Requirements

- Apache web server (others might work also)
- PHP 7+ or 8+ with OpenSSL support, curl and sodium extension
- OIDC server
- ParTCP server


## Installation

1. Create a client definition on the OIDC server. Jot down realm, client ID and
   client secret.

2. Register a new participant on the ParTCP server and appoint him as server
   administrator. Jot down participant ID and credential.

3. Copy all files and directories to a public accessible directory on the web
   server.

4. Create a directory where the ParTCP key pair shall be stored.

5. Rename `config-example.php` to `config.php` and adapt it to your needs (see
   "Configuration").

6. Call the setup routine:

   ```php
	$ sudo php setup.php
	```

7. Call the client URL in a webbrowser and ensure that there are no errors.


## Configuration

| Parameter | Description |
|-----------|-------------|
| `base_url` | URL of the client script (directory only – no file name)|
| `callback_profile2id` | Function for deriving ParTCP participant ID from OIDC user profile |
| `oidc_client_id` | ID of the OIDC client |
| `oidc_client_secret` | Secret of the OIDC client |
| `oidc_base_url` | URL pointing to the realm of the OIDC server |
| `partcp_server` | URL of the ParTCP server |
| `partcp_id` | Participant ID to be used by the key manager script |
| `partcp_credential` | Credential for submitting public key of the participant |
| `path_to_keys` | Absolute path to the directory where the ParTCP key pair shall be stored |


## Web frontend usage

Call the `index.php` script in a web browser and follow the instructions.


## API usage

Send all requests to the `api.php` script via HTTP GET. The response will be
in JSON format (content type "application/json").


### Get status information

A request without parameters will return status information about the current
user (see below). If there is no active user session, the response will contain
a single parameter `redirect` with the OIDC login URL. After logging in the
user will be redirected via HTTP 302 status code to the API script again.

**Status information**

| Attribute | Description |
|-----------|-------------|
| `username` | Login name of the current user |
| `profile` | Profile data of the current user |
| `participant_id` | Participant ID of the current user |
| `participant_status` | Status of the participant (see below) |
| `public_key` | Public key which has been submitted for the participant |
| `url_status` | URL for retrieving status information |
| `url_logout` | URL for closing the current user session |
| `url_register` | URL for registering a user on the ParTCP server for the first time |
| `url_renewal` | URL for retrieving a credential for submitting a new public key |
| `error` | Error code returned by the ParTCP server (if participant status is `unknown`) |
| `debug_info` | Full response returned by the ParTCP server (if participant status is `unknown`) |

**Participant status**

| Attribute | Description |
|-----------|-------------|
| `unregistered` | Participant has not been registered on the ParTCP server |
| `incomplete` | Participant has been registered, but has not submitted a public key yet |
| `complete` | Participant has been registered and has submitted a public key |
| `unknown` | Status information could not be retrieved (see `error` and `debug_info` for details) |


### Register participant for the first time

If the participant status is `unregistered` you can call the `url_register` URL in order
to have the key manager send a registration request to the ParTCP server. If everything
goes fine, you will get a response containing `participant_id` and `credential`. Otherwise
you will receive `error` and `debug_info`.

After successful registration you can transfer the user's public key via `key-submission`
message to the ParTCP server.


### Request credential for key submission

If the participant status is `incomplete` or `complete` you can call the `url_renewal`
URL in order to have the key manager send a key renewal request to the ParTCP server. If
everything goes fine, you will get a response containing `participant_id` and `credential`.
Otherwise you will receive `error` and `debug_info`.

After successful request you can transfer the user's public key via `key-submission`
message to the ParTCP server.

**Submitting a new public key for a participant with status 'complete' should only be
done, when the old public key is not in use anymore. Otherwise the devices using that
key will no longer be able to connect to the ParTCP on behalf of the correspondent
participant.**


### Close active session (logout)

In order to close the active session you can call the `url_logout` URL. The
session on the key manager server will be closed immediately, and you will
receive a `redirect` parameter containing a URL for doing an OIDC logout as
well. If you do not call that URL, a new session for the current user will be
started automatically the next time you call the OIDC login URL, i.e. there
will be no prompt for username and password.

