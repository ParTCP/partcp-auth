<?php

/*
	OIDC Client for ParTCP Key Management
	Copyright (C) 2022-2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

// Convert a list of profiles to a list of IDs

function debug_output ( $output ){
	global $Conf;
	if ( ! empty( $Conf['debugging'] ) ){
		file_put_contents( __DIR__ . '/debug.log', "{$output}\n", FILE_APPEND );
	}
}


require_once 'config.php';
header('Content-Type: application/json');
$request = file_get_contents('php://input');
$profiles = json_decode( $request, TRUE );
debug_output( "---\n" . date( 'Y-m-d H:i:s' ) . ' (profiles2id) - ' . $_SERVER['REMOTE_ADDR'] );
debug_output( '$request: ' . var_export( $request, TRUE ) );

if ( isset( $profiles['profiles'] ) ){
	$profiles = $profiles['profiles'];
}

if ( empty( $profiles ) ){
	die('[]');
}

debug_output( '$profiles: ' . var_export( $profiles, TRUE ) );

if ( ! is_callable( $Conf['callback_profile2id'] ) ){
	die( $request );
}

$ids = array_map( $Conf['callback_profile2id'], $profiles );
debug_output( '$ids: ' . var_export( $ids, TRUE ) );
echo json_encode( $ids );


// end of file profiles2ids.php

